# PyTorch Scholarship Challenge Nanodegree Program - Final Challenge Project

## Description

An image classifier from scratch that will identify different species of flowers.

## The Dataset

The data set contains images of flowers from 102 different species. We've provided a training set and a validation set. You can download the images from [here](https://s3.amazonaws.com/content.udacity-data.com/courses/nd188/flower_data.zip) as a zipped archive.

## Code

You can access code as a Jupyter Notebook called _Image Classifier Project_. Define, which if model should be loaded or prepare new training.

## Environment

Follow instructions from this [guide](https://github.com/udacity/deep-learning-v2-pytorch/blob/master/README.md).
